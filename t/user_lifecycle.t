use Test2::V0;

use Time::HiRes 'time';

use lib 't/lib';
use TestDB qw(txn_do_and_rollback);
use Test::Coocook;

plan(96);

my $t = Test::Coocook->new( test_data => 0 );

my $schema = $t->schema;

$schema->resultset('BlacklistEmail')
  ->add_email( my $blacklist_email = 'blacklisted@example.com', comment => __FILE__ );

$schema->resultset('BlacklistUsername')
  ->add_username( my $blacklist_username = 'blacklisted', comment => __FILE__ );

$t->get_ok('/');

$t->text_contains("first user");

$t->robots_flags_ok( { index => 0 } );

my $user1;

{
    my %userdata_ok = (
        username  => 'test',
        email     => 'test@example.com',
        password  => 's3cr3t',
        password2 => 's3cr3t',
    );

    $t->register_fails_like( { %userdata_ok, password2 => 'something-else' },
        qr/match/, "two different passwords" );

    $t->register_fails_like(
        { %userdata_ok, email => $blacklist_email },
        qr/email address is invalid or already taken/,
        "blacklisted email"
    );

    $t->register_fails_like(
        { %userdata_ok, email => uc $blacklist_email },
        qr/email address is invalid or already taken/,
        "blacklisted email in uppercase"
    );

    $t->register_fails_like(
        { %userdata_ok, username => $blacklist_username },
        qr/username is not available/,
        "blacklisted username"
    );

    $t->register_fails_like(
        { %userdata_ok, username => uc $blacklist_username },
        qr/username is not available/,
        "blacklisted username in uppercase"
    );

    # normally an organization needs an owner
    # but then we couldn't test registration of first user
    # so we trick a little
    $schema->fk_checks_off_do(
        sub {
            my $organization = $schema->resultset('Organization')->create(
                {
                    name           => "TestOrganization",
                    display_name   => "Test Organization",
                    description_md => __FILE__,
                    owner_id       => 9999,
                }
            );

            $t->register_fails_like(
                { %userdata_ok, username => $organization->name },
                qr/username is not available/,
                "organization name"
            );

            $t->register_fails_like(
                { %userdata_ok, username => uc $organization->name },
                qr/username is not available/,
                "organization name in uppercase"
            );

            $organization->delete();
        }
    );

    $t->register_ok( \%userdata_ok );
    $t->emails_count_is(1);

    $user1 = $schema->resultset('User')->find( { name => 'test' } );

    ok $user1->get_column($_), "column '$_' is set" for 'token_created';
    is $user1->get_column($_) => undef, "column '$_' is NULL" for 'token_expires';

    ok $user1->has_any_role('site_owner'),       "1st user created has 'site_owner' role";
    ok $user1->has_any_role('private_projects'), "1st user created has 'private_projects' role";

    $t->text_lacks( 'Sign up', "registration is not enabled by default" );

    $t->reload_config( enable_user_registration => 1 );
    $t->get('/');

    $t->register_fails_like( \%userdata_ok, qr/username is not available/, "existing username" );

    $t->register_fails_like(
        { %userdata_ok, username => 'TEST' },
        qr/username is not available/,
        "existing username in uppercase"
    );

    $userdata_ok{username} = 'new_user';

    $t->register_fails_like(
        \%userdata_ok,
        qr/email address is invalid or already taken/,
        "existing email address"
    );

    $t->register_fails_like(
        { %userdata_ok, email => uc $userdata_ok{email} },
        qr/email address is invalid or already taken/,
        "existing email address"
    );

    $userdata_ok{email} = 'new_user@example.com';

    txn_do_and_rollback $t->schema, sub {
        $user1->update(
            {
                new_email_fc  => $userdata_ok{email},
                token_expires => $user1->format_datetime( DateTime->now->add( hours => 12 ) )
            }
        );

        $t->register_fails_like(
            \%userdata_ok,
            qr/email address is invalid or already taken/,
            "email address that existing user wants to change to"
        );

        note "email change of existing user expires ...";
        $user1->update( { token_expires => $user1->format_datetime_now } );

        $t->register_ok( \%userdata_ok );
        $t->emails_count_is(3);
    };
}

subtest "verify email address" => sub {
    $t->get_ok_email_link_like( qr/verify/, "verify email address" );

    # TODO is GET on the link enough? maybe email clients or "security" software will fetch it?
    # - there is no point in requesting the password first
    #   because user needs to be able reset the password via e-mail
    # - maybe display a page with a POST button first?
    # - but every website I know does it with a simple GET

    # TODO find better solution than complex XPath expression and remove
    #      dependency on HTML::TreeBuilder::XPath, WWW::Mechanize::TreeBuilder
    my $node = $t->findnodes('//h2')->[0]
      or die "Can't find node in HTML tree";

    like $node->string_value => qr/sign in/i, "got redirected to login page";

    $t->input_has_value( username => $user1->name, "username is prefilled in login form" );
};

$t->clear_emails();

$t->register_ok(
    {
        username  => 'test2',
        email     => 'test2@example.com',
        password  => 's3cr3t',
        password2 => 's3cr3t',
    }
);

$t->emails_count_is(2);

$t->email_like(qr/Hi test2/);
$t->email_like(qr/Please verify/);
$t->shift_emails();

$t->email_like(qr/Hi test\b/);
$t->email_like(qr/somebody registered/i);
$t->email_like(qr/test2/);
$t->email_like(qr/example\.com/);      # contains domain part of email address
$t->email_unlike(qr/test2.+example.+com/);
$t->email_like(qr{ /user/test2 }x);    # URLs to user info pages
$t->email_like(qr{ /admin/user/test2 }x);
$t->shift_emails();

my $user2 = $schema->resultset('User')->find( { name => 'test2' } );
ok !$user2->has_any_role('site_owner'),      "2nd user created hasn't 'site_owner' role";
ok $user2->has_any_role('private_projects'), "2nd user created has 'private_projects' role";

$t->login_fails( $user1->name, 'invalid', "wrong password fails" );

$t->login_fails( $user2->name, 's3cr3t', "not yet verified user fails" );

{
    my $guard = $t->local_config_guard( login_sleep_secs => 1 );

    my $t1 = time();
    $t->login_ok( $user1->name, 's3cr3t' );
    my $t2 = time();

    cmp_ok $t2 - $t1, '>', 1, "login request took more than 1 second";
}

$t->logout_ok();
$t->login_ok( uc( $user1->name ), 's3cr3t', "login with username case-insensitive" );
$t->logout_ok();
$t->login_ok( $user1->email_fc, 's3cr3t', "login with email" );
$t->logout_ok();
$t->login_ok( uc( $user1->email_fc ), 's3cr3t', "login with email case-insensitive" );

$t->follow_link_ok( { text => 'Settings' } );

$t->submit_form_ok(
    {
        with_fields => {
            current_password => 's3cr3t',
            password         => 'P@ssw0rd',
            password2        => 'P@ssw0rd',
        },
    },
    "submit change password form"
);

$t->text_contains('Your password has been changed');

$t->email_like(qr/ password .+ changed /x);

$t->clear_emails;

$t->change_display_name_ok('John Doe');

$t->logout_ok();

$t->text_contains('logged out');

$t->get_ok('/login');

$t->robots_flags_ok( { index => 1, archive => 1 }, "plain /login may be indexed" );

$t->input_has_value( username => '', "... username is deleted from session cookie by logout" );

$t->checkbox_is_off('store_username');

$t->get_ok('/login?username=from_query');

$t->robots_flags_ok( { index => 0, archive => 0 }, "/login with query string may NOT be indexed" );

$t->input_has_value( username => 'from_query', "... username is prefilled from URL query" );

$t->checkbox_is_off('store_username');

is $t->cookie_jar->get_cookies( $t->base, 'username' ) => undef,
  "username is not stored in persistent cookie";

$t->login_ok( $user1->name, 'P@ssw0rd', store_username => 'on' );
$t->text_contains( 'Admin', "Admin section in footer" );     # issue #332
$t->content_contains( q{/admin"}, "link to admin page" );    # issue #332

$t->logout_ok();

{
    my $original_length = length $t->cookie_jar->as_string;

    # we want to clear only that specific cookie
    $t->cookie_jar->clear( 'localhost.local', '/', 'coocook_session' )
      and note "deleted session cookie";

    $original_length > length $t->cookie_jar->as_string
      or die "failed to delete session cookie";
}

$t->get_ok('https://localhost/login');

is $t->cookie_jar->get_cookies( $t->base, 'username' ) => $user1->name,
  "'username' cookie stored username";

$t->input_has_value( username => $user1->name, "username is prefilled from persistent cookie" );

$t->robots_flags_ok( { index => 0, archive => 0 },
    "/login with username from cookie may NOT be indexed" );

$t->form_number(3);
$t->checkbox_is_on('store_username');

$t->login_ok( $user1->name, 'P@ssw0rd', store_username => '' );

$t->logout_ok();

is $t->cookie_jar->get_cookies( $t->base, 'username' ) => undef,
  "... username was deleted from persistent cookie";

subtest "expired password reset token URL" => sub {
    $t->request_recovery_link_ok( $user1->email_fc );

    $schema->resultset('User')->update( { token_expires => '2000-01-01 00:00:00' } );

    $t->get_ok_email_link_like(qr/reset_password/);

    $t->text_like(qr/expired/);
    $t->text_lacks('verified');

    $t->clear_emails();
};

subtest "password recovery" => sub {
    $user1->discard_changes;
    ok $user1->check_password('P@ssw0rd'), "password is same as before";

    $t->request_recovery_link_ok( 'not-registered@example.com',
        "request recovery for unregistered email address" );
    my $response_unregistered = $t->response;

    $t->request_recovery_link_ok( uc( $user1->email_fc ), "request recovery for user1" );
    is $t->response->code    => $response_unregistered->code,    "... HTTP status code is same";
    is $t->response->content => $response_unregistered->content, "... content is same";

    $t->get_ok_email_link_like(qr/reset_password/);
    $t->text_contains('verified');

    $t->submit_form_ok(
        {
            with_fields => {
                password  => 'foo',
                password2 => 'bar',
            }
        },
        "submit two different passwords"
    );

    $t->text_like(qr/don.t match/);

    $user1->discard_changes();
    ok $user1->check_password('P@ssw0rd'), "password hasn't been changed";

    my $new_password = 'new, nice & shiny';
    $t->submit_form_ok( { with_fields => { map { $_ => $new_password } 'password', 'password2' } },
        "submit new password twice" );

    $t->text_like(qr/ password .+ changed/x);

    $user1->discard_changes();
    ok $user1->check_password($new_password), "password has been changed";

    $t->logout_ok();

    $t->clear_emails();
};

subtest "password recovery marks email address verified" => sub {
    $user2->discard_changes;
    is $user2->email_verified => undef,
      "email_verified IS NULL";

    $t->request_recovery_link_ok( $user2->email_fc );
    $t->get_ok_email_link_like(qr/reset_password/);
    $t->submit_form_ok( { with_fields => { map { $_ => 'sUpEr s3cUr3' } 'password', 'password2' } },
        "submit password reset form" );

    $user2->discard_changes;
    isnt $user2->email_verified => undef,
      "email_verified IS NOT NULL";

    $t->logout_ok();
};

$t->login_ok( $user1->name, 'new, nice & shiny' );

subtest "redirects after login/logout" => sub {
    $t->get_ok('https://localhost/about');

    $t->logout_ok();

    $t->base_is( 'https://localhost/about', "client is redirected to last page after logout" );

    # pass link around between login/register
    $t->follow_link_ok( { text => 'Sign up' } );

    # keep link with failed login attempts
    $t->login_fails( $user1->name, 'invalid' );

    note "uri: " . $t->uri;

    $t->login_ok( $user1->name, 'new, nice & shiny' );

    $t->base_is( 'https://localhost/about', "client is redirected to last page after login" );
};

subtest "refreshing login page after logging in other browser tab" => sub {
    $t->get_ok('https://localhost/login?redirect=/statistics');

    $t->base_is( 'https://localhost/statistics', "client is redirected immediately" );
};

subtest "refreshing register page after logging in other browser tab" => sub {
    $t->get_ok('https://localhost/register?redirect=/statistics');

    $t->base_is( 'https://localhost/statistics', "client is redirected immediately" );
};

subtest "malicious redirects are filtered on logout" => sub {
    $t->is_logged_in();

    $t->post('https://localhost/logout?redirect=https://malicious.example/');
    $t->base_is( 'https://localhost/', "client is redirected to /" );

    $t->is_logged_out("... but client is logged out anyway");
};

subtest "malicious redirects are filtered on login" => sub {
    $t->post(
        'https://localhost/login?redirect=https://malicious.example/',
        { username => $user1->name, password => 'new, nice & shiny' }
    );
    $t->base_is( 'https://localhost/', "client is redirected to /" );
    $t->is_logged_in();

    $t->logout_ok();
    $t->post(
        '/login',
        {
            redirect => 'https://malicious.example/',
            username => $user1->name,
            password => 'new, nice & shiny'
        }
    );
    $t->base_is( 'https://localhost/', "client is redirected to /" );
    $t->is_logged_in("... but client is logged in anyway");
};

$t->create_project_ok( { name => "Test Project 1" } );

$t->base_unlike( qr/import/, "not redirected to importer for first project" );

note "remove all roles from user";
$schema->resultset('RoleUser')->search( { user_id => '1' } )->delete;

$t->create_project_ok( { name => "Test Project 2" } );

$t->base_like( qr/import/, "redirected to importer" );

$t->text_contains("Test Project 1");

ok my $project = $schema->resultset('Project')->find( { name => "Test Project 1" } ),
  "project is in database";

ok !$project->is_public, "1st project is private";

is $project->owner->name => $user1->name,
  "new project is owned by new user";

is $project->users->one_row->name => $user1->name,
  "owner relationship is also stored via table 'projects_users'";

ok my $project2 = $schema->resultset('Project')->find( { name => "Test Project 2" } );

ok $project2->is_public, "2nd project is public";
