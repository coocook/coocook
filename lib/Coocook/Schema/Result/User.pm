package Coocook::Schema::Result::User;

use Coocook::Base qw(Moose);

use Carp;
use Coocook::Model::Token;
use DateTime;

extends 'Coocook::Schema::Result';

__PACKAGE__->table('users');

__PACKAGE__->add_columns(
    id             => { data_type => 'integer', is_auto_increment => 1 },
    name           => { data_type => 'text' },
    name_fc        => { data_type => 'text' },                              # fold cased
    password_hash  => { data_type => 'text' },
    display_name   => { data_type => 'text' },
    admin_comment  => { data_type => 'text', default_value => '' },
    email_fc       => { data_type => 'text' },
    new_email_fc   => { data_type => 'text',                        is_nullable => 1 },
    email_verified => { data_type => 'timestamp without time zone', is_nullable => 1 },
    token_hash     => { data_type => 'text',                        is_nullable => 1 },
    token_created  => { data_type => 'timestamp without time zone', is_nullable => 1 },
    token_expires  => { data_type => 'timestamp without time zone', is_nullable => 1 },
    created        => {
        data_type     => 'timestamp without time zone',
        default_value => \'CURRENT_TIMESTAMP',
        set_on_create => 1,
    },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraints(
    ['name'], ['name_fc'], ['email_fc'],
    ['password_hash'],    # passwords might be equal but salted hash MUST be unique
    ['token_hash'],
);

__PACKAGE__->has_many( roles_users => 'Coocook::Schema::Result::RoleUser', 'user_id' );

__PACKAGE__->has_many(
    owned_projects => 'Coocook::Schema::Result::Project',
    'owner_id',
    {
        cascade_delete => 0,    # users who own projects may not be deleted
    }
);

__PACKAGE__->has_many(
    organizations_users => 'Coocook::Schema::Result::OrganizationUser',
    'user_id'
);
__PACKAGE__->many_to_many( organizations => organizations_users => 'organization' );

__PACKAGE__->has_many( projects_users => 'Coocook::Schema::Result::ProjectUser', 'user_id' );
__PACKAGE__->many_to_many( projects => projects_users => 'project' );

__PACKAGE__->has_many( terms_users => 'Coocook::Schema::Result::TermsUser', 'user_id' );
__PACKAGE__->many_to_many( terms => terms_users => 'terms' );

around [ 'set_column', 'store_column' ] => sub ( $orig, $self, $column, $value ) {
    if ( $column eq 'name' ) {    # automatically set 'name_fc' from 'name'
        $self->$orig( name_fc => fc($value) );
    }
    elsif ( $column eq 'password' ) {    # support virtual 'password' column
        my $password = Coocook::Model::Token->from_string($value);

        ( $column => $value ) = ( password_hash => $password->to_salted_hash );
    }
    elsif ( $column eq 'token_hash' ) {
        if ( defined $value ) {          # automatically set 'token_created' when 'token_hash' is set
            $self->$orig( token_created => $self->format_datetime_now );
        }
        else {                           # reset when 'token_hash' is unset
            $self->$orig( token_created => undef );
            $self->$orig( token_expires => undef );
        }
    }

    return $self->$orig( $column => $value );
};

__PACKAGE__->meta->make_immutable;

sub alternative_email_valid_and_available ( $self, @args ) {
    my $other_users = $self->result_source->resultset->search( { id => { '!=' => $self->id } } );

    return $other_users->email_valid_and_available(@args);
}

sub blacklist {
    my $self = shift;

    $self->txn_do(
        sub {
            $self->result_source->schema->resultset('BlacklistEmail')->add_email( $self->email_fc, @_ );
            $self->result_source->schema->resultset('BlacklistUsername')->add_username( $self->name, @_ );
        },
        @_
    );
}

# method name defined by Catalyst::Authentication::Credential::Password
sub check_password ( $self, $password ) {
    return Coocook::Model::Token->from_string($password)->verify_salted_hash( $self->password_hash );
}

sub check_base64_token ( $self, $token ) {
    $self->token_hash
      or return;

    if ( my $expires = $self->token_expires ) {
        $expires > DateTime->now
          or return;
    }

    return Coocook::Model::Token->from_base64($token)->verify_salted_hash( $self->token_hash );
}

sub add_roles ( $self, @roles ) {

    if ( @roles == 1 and ref $roles[0] eq 'ARRAY' ) {
        @roles = $roles[0]->@*;
    }

    for my $role (@roles) {
        $self->create_related( roles_users => { role => $role } );
    }
}

sub has_any_role ( $self, @roles ) {
    my $roles = ( @roles == 1 and ref $roles[0] eq 'ARRAY' ) ? $roles[0] : \@roles;

    return $self->roles_users->results_exist( { role => { -in => $roles } } );
}

=head2 has_any_project_role( $project, @roles )

=head2 has_any_project_role( $project, \@roles )

Returns a boolen value indicating whether the user has direct
permissions on the C<$project> with any of the C<@roles> or the
user is member of any organization that has permissions
on the C<$project> with any of the C<@roles>.

C<@roles> should not be empty. If it is empty, the result
is always false.

=cut

sub has_any_project_role ( $self, $project, @roles ) {
    my $roles = ( @roles == 1 and ref $roles[0] eq 'ARRAY' ) ? $roles[0] : \@roles;

    if ( @$roles == 0 ) {
        carp "has_any_project_role() with zero roles";
        return;
    }

    return 1
      if $self->projects_users->results_exist(
        { project_id => $project->id, role => { -in => $roles } } );

    # organizations can never be owner
    return if @$roles == 1 and $roles->[0] eq 'owner';

    my $organizations_projects = $self->organizations->search_related('organizations_projects');

    return $organizations_projects->results_exist(
        { project_id => $project->id, $organizations_projects->me('role') => { -in => $roles } } );
}

sub has_any_organization_role ( $self, $organization, @roles ) {
    my $roles = ( @roles == 1 and ref $roles[0] eq 'ARRAY' ) ? $roles[0] : \@roles;

    return $self->organizations_users->results_exist(
        { organization_id => $organization->id, role => { -in => $roles } } );
}

sub is_member_of ( $self, $organization ) {
    return $self->organizations_users->results_exist( { organization_id => $organization->id } );
}

sub roles ($self) {
    return $self->roles_users->get_column('role')->all;
}

sub status_code        ($self) { ( $self->status )[0] }
sub status_description ($self) { ( $self->status )[1] }

sub status ($self) {
    if ( $self->email_verified ) {
        if ( my $token_expires = $self->token_expires ) {
            if ( DateTime->now <= $token_expires ) {
                return password_recovery => sprintf "requested password recovery link (valid until %s)",
                  $token_expires;
            }
        }

        return ok => "ok";
    }

    return unverified => "email address not yet verified with verification link";
}

1;
