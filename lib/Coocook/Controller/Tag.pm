package Coocook::Controller::Tag;

use Coocook::Base qw(Moose);

use PerlX::Maybe;

BEGIN { extends 'Coocook::Controller' }

=head1 NAME

Coocook::Controller::Tag - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=head2 index

=cut

sub index : GET HEAD Chained('/project/base') PathPart('tags') Args(0)
  RequiresCapability('view_project') {
    my ( $self, $c ) = @_;

    my $groups = $c->project->tag_groups;

    my @groups = $groups->hri->all;
    my %groups = map { $_->{id} => $_ } @groups;

    for my $group (@groups) {
        $group->{tags}       = [];
        $group->{delete_url} = $c->project_uri( $self->action_for('delete_group'), $group->{id} );
        $group->{update_url} = $c->project_uri( $self->action_for('update_group'), $group->{id} );
    }

    my $other_tags = [];

    {
        my $tags = $c->project->tags->sorted->hri;

        while ( my $tag = $tags->next ) {
            $tag->{edit_url} = $c->project_uri( $self->action_for('edit'), $tag->{id} );

            push @{ $tag->{tag_group_id} ? $groups{ $tag->{tag_group_id} }{tags} || die : $other_tags }, $tag;
        }
    }

    my @existing_tag_names       = $c->project->tags->get_column('name')->all;
    my @existing_tag_group_names = $c->project->tag_groups->get_column('name')->all;

    $c->json_stash(
        existing_names => {
            tags      => \@existing_tag_names,
            tagGroups => \@existing_tag_group_names,
        },
    );

    $c->stash(
        groups           => \@groups,
        other_tags       => $other_tags,
        create_url       => $c->project_uri( $self->action_for('create') ),
        tag_groups       => [ $c->project->tag_groups->sorted->hri->all ],
        create_group_url => $c->project_uri( $self->action_for('create_group') ),
    );
}

sub tag : Chained('/project/base') PathPart('tag') CaptureArgs(1)
  RequiresCapability('view_project') {
    my ( $self, $c, $id ) = @_;

    $c->stash( tag => $c->project->tags->find($id) || $c->detach('/error/object_not_found') );
}

sub tag_group : Chained('/project/base') PathPart('tag_group') CaptureArgs(1)
  RequiresCapability('view_project') {
    my ( $self, $c, $id ) = @_;

    $c->stash( tag_group => $c->project->tag_groups->find($id)
          || $c->detach('/error/object_not_found') );
}

sub edit : GET HEAD Chained('tag') PathPart('') Args(0) RequiresCapability('view_project') {
    my ( $self, $c ) = @_;

    my $tag = $c->stash->{tag};

    $c->stash( groups => [ $c->project->tag_groups->sorted->all ] );

    my @relationships = (
        [ articles => articles_tags => article => '/article/edit' ],
        [ dishes   => dishes_tags   => dish    => '/dish/edit' ],
        [ recipes  => recipes_tags  => recipe  => '/recipe/edit' ],
    );

    for (@relationships) {
        my ( $entity, $rel1 => $rel2, $path ) = @$_;

        my @hashrefs = $tag->search_related($rel1)->search_related($rel2)->hri->all;

        for my $hashref (@hashrefs) {
            $hashref->{url} = $c->project_uri( $path, $hashref->{id} );
        }

        $c->stash( $entity => \@hashrefs );
    }

    $c->stash(
        update_url => $c->project_uri( $self->action_for('update'), $tag->id ),
        delete_url => $c->project_uri( $self->action_for('delete'), $tag->id ),
        is_in_use  => $tag->is_in_use,
    );
}

sub delete : POST Chained('tag') Args(0) RequiresCapability('edit_project') {
    my ( $self, $c ) = @_;

    my $tag = $c->stash->{tag};
    $tag->delete;
    $c->forward('redirect');
}

sub delete_group : POST Chained('tag_group') PathPart('delete') Args(0)
  RequiresCapability('edit_project') {
    my ( $self, $c ) = @_;

    my $group = $c->stash->{tag_group};
    $group->deletable or die "Not deletable";
    $group->delete;
    $c->forward('redirect');
}

sub create : POST Chained('/project/base') PathPart('tags/create') Args(0)
  RequiresCapability('edit_project') {
    my ( $self, $c ) = @_;

    my $group;    # might be no group
    if ( my $id = $c->req->params->get('tag_group') ) {
        $group = $c->project->tag_groups->find($id);
    }

    my $tag = $c->project->create_related(
        tags => {
            maybe
              tag_group => $group,
            name => $c->req->params->get('name'),
        }
    );
    $c->forward('redirect');
}

sub create_group : POST Chained('/project/base') PathPart('tag_groups/create') Args(0)
  RequiresCapability('edit_project') {
    my ( $self, $c ) = @_;

    $c->project->create_related(
        tag_groups => {
            name    => $c->req->params->get('name'),
            comment => $c->req->params->get('comment'),
        }
    );
    $c->forward('redirect');
}

sub update : POST Chained('tag') Args(0) RequiresCapability('edit_project') {
    my ( $self, $c ) = @_;

    my $group;    # might be no group
    if ( my $id = $c->req->params->get('tag_group') ) {
        $group = $c->project->tag_groups->find($id);
    }

    my $tag = $c->stash->{tag};
    $tag->update(
        {
            name      => $c->req->params->get('name'),
            tag_group => $group,
        }
    );
    $c->forward( redirect => [$tag] );
}

sub update_group : POST Chained('tag_group') PathPart('update') Args(0)
  RequiresCapability('edit_project') {
    my ( $self, $c, $id ) = @_;

    $c->stash->{tag_group}->update(
        {
            name    => $c->req->params->get('name'),
            comment => $c->req->params->get('comment'),
        }
    );

    $c->forward('redirect');
}

sub redirect : Private {
    my ( $self, $c, $tag ) = @_;

    $c->response->redirect(
        $c->project_uri(
            $tag
            ? ( $self->action_for('edit'), $tag->id )
            : ( $self->action_for('index') )
        )
    );
}

__PACKAGE__->meta->make_immutable;

1;
