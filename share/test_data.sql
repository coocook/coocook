-- SQL code in this file MUST run in SQLite.
-- SQL code in this file MUST run in PostgreSQL.
-- SQL code in this file SHOULD be compliant to the latest ANSI/ISO standard SQL.

INSERT INTO "users"
(id,       name,    name_fc, password_hash, display_name,                   admin_comment,               email_fc,    email_verified) VALUES
( 1, 'john_doe', 'john_doe',   'SEE BELOW',   'John Doe',       'test user from test SQL', 'john.doe@example.com', CURRENT_TIMESTAMP),
( 2,    'other',    'other',    'NONE SET', 'Other User', 'other test user from test SQL',    'other@example.com', CURRENT_TIMESTAMP);

-- password: P@ssw0rd
UPDATE "users" SET password_hash = '$argon2i$v=19$m=32768,t=3,p=1$Gwe2aqtW9TbCpSosuN0O6Q$ISAlqvQF0LJNjj1KMgkBcw' WHERE id = 1;

INSERT INTO "roles_users"
(        role, user_id) VALUES
('site_owner',       1);

INSERT INTO "organizations"
(id,       name,    name_fc, display_name, owner_id, description_md) VALUES
( 1, 'TestData', 'testdata',  'Test Data',        1, 'from test data');

INSERT INTO "organizations_users"
(organization_id, user_id, role) VALUES
(              1,       1, 'owner');

INSERT INTO "projects"
(id,            name,        url_name,     url_name_fc,      description, is_public, owner_id,           created, archived) VALUES
( 1,  'Test Project',  'Test-Project',  'test-project',  'Test Project.',      TRUE,        1, CURRENT_TIMESTAMP,     NULL),
( 2,  'Side Project',  'Side-Project',  'side-project',  'Side Project.',     FALSE,        1, CURRENT_TIMESTAMP,     NULL),
( 3, 'Other Project', 'Other-Project', 'other-project', 'Other Project.',     FALSE,        2, CURRENT_TIMESTAMP,     NULL);

INSERT INTO "projects_users"
(project_id, user_id,     role) VALUES
(         1,       1,  'owner'),
(         1,       2, 'editor'),
(         2,       1,  'owner'),
(         3,       2,  'owner');

INSERT INTO "shop_sections"
(id, project_id,              name) VALUES
( 1,          1, 'bakery products'),
( 2,          1,   'milk products'),
( 9,          2,   'other product');

INSERT INTO "articles"
(id, project_id, shop_section_id, shelf_life_days, preorder_servings, preorder_workdays,            name, comment) VALUES
( 1,          1,               1,            NULL,              NULL,              NULL,         'flour',      ''),
( 2,          1,               1,            NULL,              NULL,              NULL,          'salt',  'NaCl'),
( 3,          1,            NULL,            NULL,              NULL,              NULL,         'water',      ''),
( 4,          1,               2,            NULL,              NULL,              NULL,        'cheese',      ''),
( 5,          1,            NULL,            NULL,              NULL,              NULL,          'love',      ''), -- has no unit
( 6,          2,               9,            NULL,              NULL,              NULL, 'other article',      '');

INSERT INTO "meals"
(id, project_id, position,         date,         name,                 comment) VALUES
( 1,          1,        1, '2000-01-01',  'breakfast', 'Best meal of the day!'),
( 2,          1,        1, '2000-01-02',      'lunch',                      ''),
( 3,          1,        1, '2000-01-03',     'dinner',                      ''),
( 9,          2,        1, '2000-01-01', 'other meal',                      '');

INSERT INTO "units"
(id, project_id, short_name,     long_name) VALUES
( 1,          1,        'g',       'grams'),
( 2,          1,       'kg',   'kilograms'),
( 3,          1,        'l',      'liters'),
( 4,          1,        't',        'tons'),
( 5,          1,        'p',     'pinches'), -- no conversion, (in German: Prisen)
( 6,          2,       'kg',   'kilograms'), -- other project
( 7,          1,       'ml', 'milliliters');

INSERT INTO "unit_conversions"
(unit1_id,   factor, unit2_id) VALUES
(       1,    0.001,        2), --  g to kg
(       2,    0.001,        4), -- kg to t
(       3, 1000    ,        7); --  l to ml

INSERT INTO "articles_units"
(article_id, unit_id) VALUES
(         1,       1), -- flour: g
(         1,       2), -- flour: kg
(         2,       1), -- salt: g
(         3,       3), -- water: l
(         4,       1), -- cheese: g
(         4,       2); -- cheese: kg

INSERT INTO "recipes"
(id, project_id,                 name, preparation, description, servings) VALUES
( 1,          1,              'pizza',          '',          '',        4),
( 2,          2,       'rice pudding',          '',          '',       42),
( 3,          3, 'chilled fruit soup',          '',          '',        1);

INSERT INTO "recipe_ingredients"
(id, position, recipe_id, prepare, value, unit_id, article_id,             comment) VALUES
( 1,        3,         1,   FALSE,  15.0,       1,          2,                  ''),
( 2,        2,         1,   FALSE,   1.0,       2,          1,                  ''),
( 3,        1,         1,   FALSE,   0.5,       3,          3,                  ''),
( 4,        4,         1,   FALSE,  10.0,       1,          2, 'if you like salty'); -- 2nd ingredient with same article/unit


INSERT INTO "dishes"
(id, meal_id, position, from_recipe_id,       name, servings, prepare_at_meal_id,   preparation,               description, comment) VALUES
( 1,       1,        1,           NULL, 'pancakes',        4,               NULL,            '', 'Make them really sweet!', 'sweet'),
( 2,       2,        2,              1,    'pizza',        2,               NULL,            '',                        '',      ''),
( 3,       3,        3,           NULL,    'bread',        4,                  2, 'Bake bread!',                        '',      '');

INSERT INTO "dish_ingredients"
(id, position, dish_id, prepare,  value, unit_id, article_id, comment, item_id) VALUES
( 1,        1,       1,   FALSE,  500.0,       1,          1,      '',    NULL),
( 2,        2,       1,   FALSE,    5.0,       1,          2,      '',    NULL),
( 3,        3,       1,   FALSE,    0.5,       3,          3,      '',    NULL),
( 4,        1,       2,   FALSE,    0.5,       2,          1,      '',    NULL),
( 5,        2,       2,   FALSE,   0.25,       3,          3,      '',    NULL),
( 6,        3,       2,   FALSE,   12.5,       1,          2,      '',    NULL),
( 7,        1,       3,    TRUE,    1.0,       2,          1,      '',    NULL),
( 8,        2,       3,    TRUE,   25.0,       1,          2,      '',    NULL),
( 9,        3,       3,    TRUE,    1.0,       3,          3,      '',    NULL),
(10,        4,       3,   FALSE,  500.0,       1,          4,      '',    NULL),
(11,        5,       3,   FALSE,   12.5,       2,          1,      '',    NULL);

INSERT INTO "purchase_lists"
(id, project_id,          name,         date) VALUES
( 1,          1, 'all at once', '1999-12-31'),
( 2,          1, 'not to buy',  '2000-01-01');

UPDATE "projects" SET default_purchase_list_id = 1 WHERE id = 1;

INSERT INTO "items"
(id, purchase_list_id, value, "offset", unit_id, article_id, purchased,              comment) VALUES
( 1,                1,  14.5,      0.0,       2,          1,     FALSE,                   ''),
( 2,                1,  42.5,     +7.5,       1,          2,     FALSE, 'rounded to integer'),
( 3,                2,  1.75,      0.0,       3,          3,     FALSE,                   ''),
( 4,                1, 500.0,      0.0,       1,          4,     FALSE,                   '');

-- items are handcrafted to match items.id == article_id -> assigning items is simple:
UPDATE "dish_ingredients" SET item_id = article_id;

INSERT INTO "tag_groups"
(id, project_id,    color,        name,    comment) VALUES
( 1,          1, 16711680, 'allergens', 'may harm'); -- 16711680 = 0xff0000 (red)

INSERT INTO "tags"
(id, project_id, tag_group_id,        name) VALUES
( 1,          1,            1,    'gluten'),
( 2,          1,            1,   'lactose'),
( 3,          1,         NULL, 'delicious');

INSERT INTO "articles_tags"
(article_id, tag_id) VALUES
(         1,      1),
(         4,      2);

INSERT INTO "dishes_tags"
(dish_id, tag_id) VALUES
(      1,      3);

INSERT INTO "recipes_tags"
(recipe_id, tag_id) VALUES
(        1,      3);

INSERT INTO "faqs"
(id, position, anchor,                                        question_md,                                                                    answer_md) VALUES
( 1,        2, 'foss', 'Is Coocook free and open-source software (FOSS)?',                                                                'Yes, it is.'),
( 2,        1, 'what',                                 'What is Coocook?', 'Coocook is a web application for collecting recipes and making food plans.');

INSERT INTO "terms"
(id,   valid_from,                                 content_md) VALUES
( 1, '1999-01-01',       'All your recipes are belong to us.'),
( 2, '2100-01-01', 'Just STEAL ALL THE COOKING INSTRUCTIONS!');

INSERT INTO "blacklist_usernames"
(       "comment", "username_fc", "username_type") VALUES
( 'test_data.sql',   '*coocook*',      'wildcard'),
( 'test_data.sql',       'admin',     'cleartext');

INSERT INTO "blacklist_emails"
(       "comment",                                     "email_fc", "email_type") VALUES
( 'test_data.sql', 'eH1bfAbKCiHJZDbMfIEX5v4EbQ/X3tyujJO/wUuOXfc=', 'sha256_b64'), -- somebody@example.com
( 'test_data.sql',                            '*@coocook.example',   'wildcard'),
( 'test_data.sql',                                '*@coocook.org',   'wildcard'),
( 'test_data.sql',                              '*@*.coocook.org',   'wildcard');
