[%
js.push('/lib/coocook-web-components/dist/autocomplete/autocomplete.es.js');
js.push('/js/tagAutocomplete.js');
js_push_template_path();

IF article; escape_title( 'Article', article.name ); ELSE; title="New Article"; END %]

<form method="post" action="[% submit_url %]" class="mb-3">

    <div class="d-flex gap-2 mb-3">
        <div class="form-floating flex-grow-1">
            <input type="text" id="name" name="name" placeholder="name" class="form-control" value="[% article.name | html %]" required>
            <label for="name">Name *</label>
        </div>
        <div class="form-floating flex-grow-1">
            <input type="text" id="comment" name="comment" placeholder="comment" class="form-control" value="[% article.comment | html %]">
            <label for="comment">Comment</label>
        </div>
        <div class="form-floating flex-grow-1">
            <select id="shop_section" name="shop_section" class="form-select">
                <option value="" style="font-style:italic">(no shop section)</option>
                [% FOREACH section IN shop_sections %]
                    <option value="[% section.id %]" [% 'selected' IF section.id == article.shop_section.id %]>[% section.name | html %]</option>
                [% END %]
            </select>
            <label for="shop_section">Shop section</label>
        </div>
    </div>

    <hr>

    <div class="container d-flex flex-column gap-3">
        <div class="row">
            <div class="col-2 d-flex align-items-center" style="min-width: 180px;">
                Suggested units
                <i class="material-icons md-18" style="color:rgb(251, 220, 53)">star</i>
            </div>
            <div class="col-10 d-flex gap-2 flex-wrap">
            [% FOR unit IN units;
                ingredients_plus_items = article.id ?   unit.number_of_dish_ingredients
                                                      + unit.number_of_recipe_ingredients
                                                      + unit.number_of_items
                                                    : undef %]
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="[% unit.id %]" id="u[% unit.id %]" name="units" [% 'checked' IF unit.selected %]>
                    <label class="form-check-label" for="u[% unit.id %]" style="[% 'font-weight: bold' IF ingredients_plus_items %]">
                        [% display_unit(unit, {html => 1});

                        IF ingredients_plus_items %]
                            <span title="used for [%
                                IF unit.number_of_recipe_ingredients > 0;
                                    numerus(unit.number_of_recipe_ingredients, 'recipe ingredient', 'recipe ingredients');
                                    ' and ' IF unit.number_of_dish_ingredients > 0
                                            OR unit.number_of_items > 0;
                                END;

                                IF unit.number_of_dish_ingredients > 0;
                                    numerus(unit.number_of_dish_ingredients, 'dish ingredient', 'dish ingredients');
                                    ' and ' IF unit.number_of_items > 0;
                                END;

                                IF unit.number_of_items > 0;
                                    numerus(unit.number_of_items, 'purchase list item', 'purchase list items');
                                END %]">([% ingredients_plus_items %])</span>
                        [% END %]
                    </label>
                </div>
            [% END %]
            </div>
        </div>
        <div class="row">
            <div class="col-2 d-flex align-items-center" style="min-width: 180px;">
                <div class="form-check d-flex gap-2 align-items-center">
                    <input class="form-check-input" type="checkbox" name="preorder" id="preorder" [% 'checked' IF article.preorder_workdays.defined %]>
                    <label class="form-check-label" for="preorder">preorder</label>
                </div>
            </div>
            <div class="col-10 d-flex gap-2 align-items-center">
                <input id="preorder_workdays" class="form-control" type="number" min="0" name="preorder_workdays" value="[% article.preorder_workdays OR default_preorder_workdays %]" style="width: 100px;">
                <label for="preorder_workdays" class=" text-nowrap">business days in advance</label>

                <span>if</span>
                <input id="preorder_servings" class="form-control" type="number" min="1" name="preorder_servings" value="[% article.preorder_servings OR default_preorder_servings %]" style="width: 100px;">
                <label for="preorder_servings" class=" text-nowrap">servings or more</label>
            </div>
        </div>
        <div class="row">
            <div class="col-2 d-flex align-items-center" style="min-width: 180px;">
                <div class="form-check d-flex gap-2">
                    <input class="form-check-input" type="checkbox" name="shelf_life" id="shelf_life" [% 'checked' IF article.shelf_life_days.defined %]>
                    <label class="form-check-label flex-grow-1 text-nowrap" for="shelf_life">known shelf life</label>
                </div>
            </div>
            <div class="col-10 d-flex gap-2 align-items-center">
                <span>of</span>
                <input id="shelf_life_days" class="form-control" type="number" min="0" name="shelf_life_days" value="[% article.shelf_life_days.defined ? article.shelf_life_days : default_shelf_life_days %]" style="width: 100px;">
                <label for="shelf_life_days">days</label>
            </div>
        </div>
        <div class="row">
            <div class="col-2 d-flex align-items-center" style="min-width: 180px;">
                Tags
            </div>
            <div class="col-10">
                <cc-multi-autocomplete
                  id="tags"
                  class="tags"
                  type="search"
                  name="tags"
                  value="article_tags"
                  static-base-uri="[% uri_for_static('/') %]"
                ></cc-multi-autocomplete>
            </div>
        </div>
    </div>

    <hr>

    <input class="btn btn-primary" type="submit" value="[% article ? 'Update' : 'Create' %]">

</form>

[% IF article %]
<h2>Where <em>[% article.name | html %]</em> is used</h2>

<ul>
[% FOR r IN recipes %]
    <li>recipe <a href="[% r.recipe.url %]">[% r.recipe.name | html %]</a>
    <ul>
    [% FOR dish IN r.dishes %]
        <li>dish <a href="[% dish.url %]">[% dish.name | html %]</a> on [% display_date(dish.meal.date, {html=>1}) %]</li>
    [% END %]
    </ul>
    </li>
[% END %]

[% FOR dish IN dishes %]
    <li>dish <a href="[% dish.url %]">[% dish.name | html %]</a> on [% display_date(dish.meal.date, {html=>1}) %]</li>
[% END %]
</ul>
[% END;

WRAPPER includes/infobox.tt %]
    Autocomplete will prioritize <em>suggested units</em> when adding an ingredient
    with this article to a recipe/dish. Other units may be used, too.
</p>

<p>
    <em>Known shelf life</em> helps purchasing articles not too early for them to last until they’re used.
[% END %]
