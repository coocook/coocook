[%~ MACRO css_push_template_path BLOCK;
    css.push( '/css/' _ template.name.replace('\.tt$', '.css') );
END;

MACRO js_push_template_path BLOCK;
    js.push( '/js/' _ template.name.replace('\.tt$', '.js') );
END;

MACRO display_date(date, opts) BLOCK;
    IF opts.html ~%]
        <abbr title="[% date.strftime(date_format_long) %]">[% date.strftime(date_format_short) %]</abbr>
    [%~ ELSE;
        date.strftime( opts.short ? date_format_short : date_format_long );
    END;
END;

MACRO display_datetime(datetime, opts) BLOCK;
    datetime.strftime( opts.short ? datetime_format_short : datetime_format_long );
END;

MACRO display_project(project, opts) BLOCK ~%]
    <i class="material-icons [% opts.icon_class || 'md-18' %]">[% project.is_public ? 'public' : 'lock' %]</i>&thinsp;
    [% project.name | html;
END;

MACRO display_tag(tag) BLOCK ~%]
    <span class="btn btn-outline-secondary btn-sm pe-none" style="font-size: 80%">[% tag.name | html %]</span>
[% END;

MACRO display_unit(unit, opts) BLOCK;
    IF opts.html ~%]
        <abbr title="[% unit.long_name | html %]">[% unit.short_name | html %]</abbr>
    [%~ ELSIF opts.print;
        unit.long_name | html;
    ELSE;
        unit.short_name | html;
        ' (';
        unit.long_name | html;
        ')';
    END;
END;

MACRO display_value(value, opts) BLOCK;
    '−' IF value < 0;    # > non-ASCII minus https://www.compart.com/de/unicode/U+2212
    IF opts.force_sign;
        '±' IF value == 0;
        '+' IF value > 0;
    END;
    IF value < 0;    # > heal broken syntax highlighting
        value = 0 - value;
    END;
    USE SignificantDigits;
    value | significant_digits;
END;

MACRO display_value_unit(value, unit, opts) BLOCK;
    display_value(value, opts);
    '&thinsp;';
    display_unit(unit, opts);
END;

MACRO link_organization(organization) BLOCK;
    THROW "no URL" UNLESS organization.url ~%]
    <i class="material-icons">groups</i>&thinsp;<a href="[% organization.url %]" title="Show organization">[% organization.display_name | html %]</a>
    (<span class="organization">[% organization.name %]</span>)
[%~ END;

MACRO link_user(user) BLOCK;
    THROW "no user URL" UNLESS user.url ~%]
    <i class="material-icons">person</i>&thinsp;<a href="[% user.url %]" title="Show user profile">[% user.display_name | html %]</a> (<span class="username">[% user.name %]</span>)
[%~ END;

MACRO link_project(project, opts) BLOCK;
    THROW "no project URL" UNLESS project.url || opts.url ~%]
    <i class="material-icons [% opts.icon_class || 'md-18' %]">[% project.is_public ? 'public' : 'lock' %]</i>&thinsp;
    <a href="[% opts.url || project.url %]" class="[% opts.class %]">[% project.name | html %]</a>
[%~ END;

MACRO numerus(number, singular, plural, opts) BLOCK; # apply correct grammatical number
    IF opts.significant_digits;
        USE SignificantDigits;
        number | significant_digits;
    ELSE;
        number;
    END;
    ' ';
    opts.infix _ ' ' IF opts.infix;
    number == 1 ? singular : plural;
END;

BLOCK table ~%]
<table class="table [% 'table-striped' IF length >= 4 %] [% 'table-hover' IF length >= 2 %] [% classes %]">
    [% content %]
</table>
[%~ END;

MACRO list(items, block, opts) BLOCK;
length=items.size ~%]
<ul class="list-group [% 'list-group-striped' IF length >= 4 %] [% 'list-group-hover' IF length >= 2 %] [% opts.list_classes %]">
[% FOR item IN items %]
    <li class="list-group-item [% opts.item_classes %]">
        [% PROCESS $block item=item %]
    </li>
[% END %]
</ul>
[%~ END ~%]
